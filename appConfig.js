// Specifics for particular entities
const appConfig = [
    {
        name: 'Trade Me LSS',
        requestsFilter: "request.uri = '/listings/search'", // for NRQL WHERE clause
        maxLatency: 3000, // ms
        availabilityPercentage: 99.95 // %
    }
];


export const getConfig = () => {
    return appConfig;
};