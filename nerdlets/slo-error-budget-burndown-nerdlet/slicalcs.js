import uuidv4 from 'uuid';
import cloneDeep from 'lodash/cloneDeep';


export const calculateBudgetAndTotals = (originalTotalsData, successSLO) => {
    const totalsData = cloneDeep(originalTotalsData);
    const requestsData = totalsData.find(d => d.metadata.name === 'Requests');
    const errorsDataSeries = totalsData.filter(d => d.metadata.name !== 'Requests');

    const successfulTotalCount = requestsData.data[0].y;
    const budget = Math.floor((1-successSLO) * successfulTotalCount);

    // Append budget and spend series to chartable data
    // seemingly unnecessary data fields are omitted (begin_time, end_time, y as `seriesName`)
    const chartData = {
        metadata: {
            name: 'Budget allocated',
            label: 'Error budget allocated',
            id: uuidv4(),
            viz: 'main',
            color: '#000000',
            tooltip: `Errors allowed before exceeding ${successSLO} success SLO`,
            units_data: {
                x: "TIMESTAMP",
                y: "COUNT" // enables rounding to higher order units (i.e. M, k)
            }
        },
        data: [
            {
                x: 0,
                y: budget
            }
        ]
    };
    totalsData.push(chartData);
    totalsData.push(generateRemainingErrorBudget(errorsDataSeries, budget));

    return {
        budget,
        totalsData
    }
}


export const generateRemainingErrorBudget = (errorData, budget) => {
    const chartData = {
        metadata: {
            name: 'Budget remaining',
            label: 'Error budget remaining',
            id: uuidv4(),
            viz: 'main',
            color: '#ff00dc',
            units_data: {
                x: "TIMESTAMP",
                y: "COUNT"
            }
        },
        data: []
    };
    let remainingBudget = budget;

    for (let i = 0; i < errorData[0].data.length; i++) {
        let errors = errorData.reduce((total, series) => { return total + series.data[i].y; }, 0);

        remainingBudget = remainingBudget - errors;
        chartData.data.push({
            x: errorData[0].data[i].x,
            y: remainingBudget
        });
    }

    return chartData;
}