import React from 'react';
import PropTypes from 'prop-types';
import { ChartGroup, Grid, GridItem, BillboardChart, LineChart, NrqlQuery, Spinner, BlockText, HeadingText, Tooltip, Icon } from 'nr1';
import { calculateBudgetAndTotals, generateRemainingErrorBudget } from './slicalcs.js';
import { getConfig } from "../../appConfig";

// https://docs.newrelic.com/docs/new-relic-programmable-platform-introduction

const appConfig = getConfig();

export default class ServiceLevelIndicators extends React.Component {
    static propTypes = {
        platformUrlState: PropTypes.object,
        entity: PropTypes.object
    }

    constructor(props) {
        super(props);
        console.debug("Nerdlet props", this.props);
        const thisAppGuid = this.props.entity.guid;
        this.accountId = this.props.entity.accountId;
        this.successSLOpercentage = 0.99;
        this.successSLOlatency = 10;
        this.appRequestFilter = `entityGuid = '${thisAppGuid}'`;
        this.applyOverrides(this.props.entity.name);

        this.state = {};
    }
    
    applyOverrides(name){
        const app = appConfig.find(a => a.name === name);
        if (app) {
            console.debug(`Applying ${name} configuration `, app)
            if (app.requestsFilter) {
                this.appRequestFilter += `AND ${app.requestsFilter}`
            }
            if (app.availabilityPercentage) {
                this.successSLOpercentage = app.availabilityPercentage/100;
            }
            if (app.maxLatency) {
                this.successSLOlatency = app.maxLatency/1000;
            }
        }
    }

    sinceNRQL(platformUrlState) {
        let since = '';
        if (platformUrlState.timeRange.begin_time) {
            since = ` SINCE ${platformUrlState.timeRange.begin_time} `;
            if (this.props.platformUrlState.timeRange.end_time) {
                since += ` UNTIL ${platformUrlState.timeRange.end_time} `;
            }
        } else if(platformUrlState.timeRange) {
            const { duration } = platformUrlState.timeRange;
            since = ` SINCE ${duration/1000/60} MINUTES AGO `;
        }
        return since;
    }

    renderProcessingPlaceholder(loading, error) {
        const foucHeightFix = {height: '300px'};
        if (loading) {
            return <Spinner style={foucHeightFix} fillContainer />;
        }
        if (error) {
            return <BlockText style={foucHeightFix}>{error.message}</BlockText>;
        }
    }

    renderHintIfNoConfiguration(appName) {
        if(appConfig.find(a => a.name === appName)) {
            return <React.Fragment/>
        }
        const tip = `No configuration found for ${appName}, assuming default SLO`
        return <Tooltip text={tip}>&nbsp;<Icon type={Icon.TYPE.INTERFACE__INFO__INFO} /></Tooltip>
    }

    updateTotalsForRange(since, onUpdateCallback) {
        if (since != this.state.since) {
            const totalsNrql = `SELECT count(*) as 'Requests', count(error) AS 'Errors', filter(count(*), WHERE duration >= ${this.successSLOlatency}) AS '> ${this.successSLOlatency} seconds' FROM Transaction WHERE ${this.appRequestFilter}`;

            NrqlQuery.query({accountId: this.accountId, formateType: NrqlQuery.FORMAT_TYPE.RAW, query: totalsNrql+since}).then((totalsForTimeRange) =>{
                if (!totalsForTimeRange.data.loading) {
                    const { budget, totalsData } = calculateBudgetAndTotals(totalsForTimeRange.data.chart, this.successSLOpercentage);
                    console.debug(`Calculated error budget ${budget} for total requests data`, totalsData);

                    this.setState({
                        since: since,
                        budget: budget,
                        totalsData: totalsData
                    });

                    onUpdateCallback()
                }
            });
        }
    }

    updateBurndownForRange(){
        const errorsNrql = `SELECT count(error) AS 'Errors', (count(*) - count(error)) AS '> ${this.successSLOlatency} seconds' FROM Transaction WHERE ${this.appRequestFilter} AND (duration >= ${this.successSLOlatency} OR error) TIMESERIES MAX`;
        NrqlQuery.query({accountId: this.accountId, formateType: NrqlQuery.FORMAT_TYPE.RAW, query: errorsNrql+this.state.since}).then((burndownForTimeRange) =>{
            console.debug(`Retrieved errors ${this.state.since}`, burndownForTimeRange.data.chart);
            if (!burndownForTimeRange.data.loading) {
                
                console.debug(`Generate burndown remaining for a budget total of: ${this.state.budget}`);
                burndownForTimeRange.data.chart.push(generateRemainingErrorBudget(burndownForTimeRange.data.chart, this.state.budget));

                this.setState({
                    burndownData: burndownForTimeRange.data.chart
                });
            }
        });
    }

    render() {
        const since = this.sinceNRQL(this.props.platformUrlState);

        // Retrieve totals and calculate error budget, then the burndown chart data
        this.updateTotalsForRange(since, this.updateBurndownForRange.bind(this));

        const requestsNrql = `SELECT count(*) as 'Requests' FROM Transaction WHERE ${this.appRequestFilter} TIMESERIES MAX`;

        return  (
            <Grid className="grid" spacingType={[Grid.SPACING_TYPE.SMALL]}>
                <GridItem className="panel" columnSpan={12}>
                    <HeadingText type={HeadingText.TYPE.Heading_3}>Service Level Objective: {this.successSLOpercentage * 100}% of requests will successfully complete within {this.successSLOlatency} seconds{this.renderHintIfNoConfiguration(this.props.entity.name)}</HeadingText>
                    <BillboardChart
                        className=""
                        data={this.state.totalsData}
                        accountId={this.accountId}
                        fullWidth
                    />
                </GridItem>
                <ChartGroup>
                <GridItem className="panel" columnSpan={12}>
                <HeadingText type={HeadingText.TYPE.Heading_3}>Error budget burndown</HeadingText>
                    <LineChart
                        className="chart"
                        data={this.state.burndownData}
                        accountId={this.accountId}
                        />
                </GridItem>
                <GridItem className="panel" columnSpan={12}>
                    <HeadingText type={HeadingText.TYPE.Heading_3}>Service requests</HeadingText>
                    <NrqlQuery accountId={this.accountId} query={requestsNrql+since}>
                        {({data, loading, error}) => {
                            const notLoaded = this.renderProcessingPlaceholder(loading, error);
                            if (notLoaded) return notLoaded;

                            return <LineChart
                                className="chart"
                                data={data}
                                accountId={this.accountId}
                            />
                        }}
                    </NrqlQuery>
                </GridItem>
                </ChartGroup>
            </Grid>
        );
    }
}
