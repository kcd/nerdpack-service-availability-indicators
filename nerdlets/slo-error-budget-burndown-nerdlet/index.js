import React from 'react';
import ServiceLevelIndicators from './main';
import { PlatformStateContext, nerdlet,Dropdown, DropdownItem, Grid, GridItem, HeadingText, NerdletStateContext, EntitiesByDomainTypeQuery, EntityByGuidQuery, Spinner, BlockText } from 'nr1';
import { getConfig } from "../../appConfig";

const appConfig = getConfig();

export default class Wrapper extends React.PureComponent {

    setApplication(appName, appGuid) {
        console.debug("Loading application entity",appName, appGuid);
        nerdlet.setUrlState({
            appName:appName,
            entityGuid:appGuid,
            showSelector:true
        });
    }

    hasConfig(name){
        return appConfig.find(a => a.name === name) != null;
    }

    _renderApplicationSelector(data) {
        const sorted = data.sort((a,b)=> {
            const aConfigured = this.hasConfig(a['name'])
            const bConfigured = this.hasConfig(b['name'])
            if ( aConfigured === bConfigured) {
                //alphabetical
                return a['name'] < b['name'] ? -1 : 1
            }
            // but configured apps first
            return aConfigured ? -1 : 1;
        })
        return(
            <Dropdown title="Select application">
            {sorted.map((item) => {
                return <DropdownItem key={item['guid']} onClick={() => (this.setApplication(item['name'], item['guid']))}>{item['name']}</DropdownItem>
            })}
            </Dropdown>
        )
    }

    render() {
        return (
            <PlatformStateContext.Consumer>
                {platformUrlState => (
                    <NerdletStateContext.Consumer>
                        {nerdletUrlState => {
                             console.debug("nerdletUrlState", nerdletUrlState)

                            return <React.Fragment>
                                {(nerdletUrlState.entityGuid == null || nerdletUrlState.showSelector) &&
                                <EntitiesByDomainTypeQuery entityDomain="APM" entityType="APPLICATION">
                                    {({loading, error, data}) => {
                                        console.debug('Domain entities loaded', [loading, data, error]);
                                        if(loading) {
                                            return <Spinner/>;
                                        }
                                        if (error) {
                                            return <BlockText>{error.message}</BlockText>
                                        }
                                        return <Grid>
                                            <GridItem columnSpan={10}><HeadingText type={HeadingText.TYPE.HEADING_1} className='apptitle'>{nerdletUrlState.appName}</HeadingText></GridItem>
                                            <GridItem columnSpan={2} className="rightalign">{this._renderApplicationSelector(data.entities)}</GridItem>
                                        </Grid>
                                    }}
                                </EntitiesByDomainTypeQuery>
                                }
                                {nerdletUrlState.entityGuid &&
                                <EntityByGuidQuery entityGuid={nerdletUrlState.entityGuid}>
                                {({ loading, error, data }) => {
                                    if (loading) {
                                        return <Spinner fillContainer />;
                                    }
                                    if (error || data.count < 1) {
                                        return <BlockText>Failed to load entity [{nerdletUrlState.entityGuid}]: {error && error.message || ''}</BlockText>;
                                    }
                                    return (
                                        < ServiceLevelIndicators
                                            platformUrlState = {platformUrlState}
                                            entity = {data.entities[0]}
                                        />
                                    );
                                }}
                            </EntityByGuidQuery>}</React.Fragment>

                    }}
                    </NerdletStateContext.Consumer>
                )}
            </PlatformStateContext.Consumer>
        );
    }
}

